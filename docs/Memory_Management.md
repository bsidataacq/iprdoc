#

## Memory Management

Starting with IceRadar ver 6 and X.IceRadar ver 1, data files have attributes associated to the "File" root object. These can be used to identify changes in the file format (presently only for some object's attributes) so third-party routines can be adjusted to different data file versions. 

- H5DataVersion
- HardwareID
- SourceApplication

<img src="../img/fileversion_dual.png" align="center">

There is no attribute for the file object in  previous versions.


|H5DataVersion | IceRadar          | X.IceRadar          | AirIPR        | Comments |
|--------------| ------------------|---------------------|---------------|----------|
|2.0.0         |  up to ver 6.0.1b | n/a                 |   n/a         |          |
|3.0.0         |  n/a              | v 1.0               |   n/a         |  (1)     |
|3.1.0         |  n/a              | n/a                 |   v 1.0       |  (2)     |

(1):  `../location_i/datacapture_0/echogram_0` and `../location_i/datacapture_1/echogram_1` present in lines acquired in dual mode 

(2): Superceded on (1) by adding AirIPR additional data streams, **but with only single mode (single polarization)** data set. 



## File Format Additional Information

### Attributes Groups seen from HDF5View utility:

Note the syntax corrections (summarized in the next section)
####-  Up to IceRadar ver 6.0.1b 
<img src="../img/hdf5view_legacy.jpg" align="center">  

####- for X-IPR ver 1.0
 With dual channel input in this example so `echogram_0`  and `echogram_1` have attributes. If only one channel input is enabled, only `echogram_0` will have attributes.
<img src="../img/hdf5view_xipr.jpg" align="center">


####- for AirIPR ver 1.0
<img src="../img/hdf5view_airipr.jpg" align="center">

### Attributes Groups Summary 

!!! Note
	 The * denotes a change as either a **space character removed**, or a **character's case changed**.
	 These can be accomodated by (1): removing all space when reading attribute names, and (2) converting all characters to lower case.
	 
	 ! denotes a **change of at least one character**. These would require a change in the reading routine to accomodate this altered variable name.


|Up to IceRadar ver 6.0.1b |--> X-IPR ver1.0 |--> AirIPR ver 1.0|Notes |
|--------------| ------------------|---------------------|---------|
|`Digitizer-MetaData_xml`| * `DigitizerMetaData_xml`|`DigitizerMetaData_xml`|         |
|`GPS Cluster- MetaData_xml`|* `GPSData_xml`|`GPSData_xml`||
|`GPS Cluster_UTM-MetaData_xml`|* `GPS_UTMData_xml`|`GPS_UTMData_xml`| |
|`PCSavetimestamp`|* `PCSaveTimestamp`|`PCSaveTimestamp`||
|						|					| *`AGLData_xml`||

### Variables Names within each Attributes Groups: 

	

|Digitizer-MetaData_xml (<= 6.0.1b)| --> DigitizerMetaData_xml (X-IPR,AirIPR)| Notes |
|--------------| ------------------|---------------------|
|`Digitizer setting`|* `DigitizerSettings`|  |
|`vertical range`|`VoltageRange`|  |
|`Vertical Offset`|* `VoltageOffset`|  |
|`vertical coupling`|* `VerticalCoupling`|  |
|`Channel`|`Channel`|  |
|`maximum input frequency`|* `MaxInputFrequency`|  |
|`  Sample Rate` (prepended with a space)|* `SampleRate`|  |
|`Record Length`|* `RecordLength`||
|`trigger type`|* `TriggerType`| |
|`trigger delay`|* `TriggerDelay`|  |
|`reference position`|* `ReferencePosition`|  |
|`trigger level`|* `TriggerLevel`|  |
|`hysteresis`|* `Hysteresis`|  |
|`low level`|* `LowLevel`|  |
|`high level`|* `HighLevel`|  |
|`trigger coupling`|* `TriggerCoupling`|  |
|`trigger window mode`|* `TriggerWindowMode`|  |
|`trigger slope`|* `TriggerSlope`|  |
|`trigger source`|* `TriggerSource`|  |
|`Trigger Modifier`|* `TriggerModifier`|  |
|`channel name`|* `ChannelName`|  |
|`Stacking` Not in files < 5.2 |`Stacking`|to verify ver. |
|`Radargram extra info`|* `RadargramExtraInfo`|  |
|`relativeInitialX`|* `RelativeInitialX`|  |
|`xIncrement`|* `XIncrement`|  |


|GPS Cluster- MetaData_xml (<= 6.0.1b)| --> GPSData_xml (X-IPR,AirIPR) | Notes |
|--------------| ------------------|---------------------|
|`GPS_timestamp_UTC`|* `GPSTimestamp_UTC`|  |
|`Lat`|`Lat`|  |
|`Long`|`Long`|  |
|`Fix_Quality`|* `FixQuality`|  |
|`Num _Sat`|* `NumSat`|  |
|`Dilution`|`Dilution`|  |
|`Alt_asl_m`|* `Alt_ASL_m`|  |
|`Geoid_Heigh_m` typo: t missing| !`GeoidHeight_m`|  |
|`GPS Fix valid`|* `GPSFixValid`|  |
|`GPS Message ok`|* `GPSMessageOk`|  |


|GPS Cluster_UTM-MetaData_xml (<= 6.0.1b) | --> GPS_UTMData_xml (X-IPR,AirIPR) | Notes |
|--------------| ------------------|---------------------|
|`Datum`|`Datum`|  |
|`Easting_m`|`Easting_m`|  |
|`Northing_m`|`Northing_m`|  |
|`Elevation`| !`Alt_ASL_m`| |
|`Zone`|`Zone`|  |
|`Satellites (dup)`| !`NumSat`|  |
|`GPS Fix Valid (dup)`|!`GPSFixValid`| |
|`GPS Message ok (dup)`| !`GPSMessageOK`| |
|`Flag_1`|`Flag_1`|  |
|`Flag_2`|`Flag_2`|  |


|AGLData_xml | Notes |
|--------------|-----|
|`Data` | 1 digit precision (0.1m) |
|`Unit` |  meter |
|`Sensor` | FMCW_v1  |
|`Valid`  | 0: false, 1: true  |

|PCSavetimestamp | PCSaveTimestamp | Notes |
|--------------| ------------------|---------------------|
