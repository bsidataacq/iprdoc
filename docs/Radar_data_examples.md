#

### Introduction

The following datasets were shared by research groups operating the IceRadar system in various part of the world using the system on a broad range of ice conditions. Unless specified otherwise all data were acquired using a *Nugen* Narod Transmitter, the IceRadar EPU, and resistively loaded dipoles antennas. Some data were acquired by skis, foot, and snow machine. 

Left vertical axis is two-way travel time (s), right axis is depth (m). Horizontal axis is sample number not geospationaly resampled.


###Ice-Shelves, Antartica


Nansen ice-shelf . Cold ice, no obvious englacial scattering. A "clean" radargram. 20 MHz center frequency antennas. 
Credit: <a href="https://wirl.carleton.ca/" rel="nofollow" target="_blank">WIRL</a>

<img src="../img/nis15.h5_line_8_v1.jpg" align="center">


The same data set with different filter settings to enhance englacial layers and upper-layers scattering.


<img src="../img/nis15.h5_line_8_v2.jpg" align="center">


###Mainland, Antarctica

Deep cold ice. Filtering for bed enhancement.  5 MHz center frequency.
Credit: <a href="https://climatechange.umaine.edu/" rel="nofollow" target="_blank">Climate Change Institute- UMaine</a>

<img src="../img/12-27-18.h5_line_3_v1.jpg" align="center">


The same data set with filter settings enhancing englacial layers and folds.

<img src="../img/12-27-18.h5_line_3_v2.jpg" align="center">


### Axel Heiberg island, Canada

Cold ice. Intricate bed structure. 20MHz center frequency.
Credit: <a href="http://www.sfu.ca/~gflowers/" rel="nofollow" target="_blank">SFU Glaciology Group</a> and <a href="https://www.queensu.ca/geographyandplanning/people/faculty/laura-thomson" rel="nofollow" target="_blank">Queen's U. Glaciology.</a>

<img src="../img/WG_4May2017_1.h5_line_4.jpg" align="center">

###Yukon, Canada

Unprocessed radargram on Kaskawulsh GL. Polythermal ice assumed. 5 MHz center frequency.
Credit: <a href="http://www.sfu.ca/~gflowers/" rel="nofollow" target="_blank">SFU Glaciology Group</a>

<img src="../img/14jul2018.h5_line_1_v1.jpg" align="center">

Same radargram processed to remove upper englacial scattering and enhanced glacier bed.

<img src="../img/14jul2018.h5_line_1_v2.jpg" align="center">

Deeper cross-section and difficult bed to extract nearing 1000m depth.

<img src="../img/15jul2018.h5_line_0.jpg" align="center">


###Katla volcano, Iceland
Temperate ice over cauldrons on Mýrdalsjökull, 2.5 MHz center frequency.
Credit: <a href="http://earthice.hi.is/about_institute" rel="nofollow" target="_blank">Earth Science Institute, University of Iceland</a>

<img src="../img/myrdk10og11_20180309_NewSystemData.h5_line_2.jpg" align="center">

###Coast Mountains, Canada

Temperate ice, 10 MHz center frequency. First example with no filtering showing strong attenuation and englacial scattering.
Credit: BSI dataset.

<img src="../img/upper nearkm 69-68.h5_line_1_v1.jpg" align="center">

Same dataset with filter set to remove scattering and enhance bed.

<img src="../img/upper nearkm 69-68.h5_line_1_v2.jpg" align="center">



