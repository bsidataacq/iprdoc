<img src="../img/equipment_1.png" align="center">

#

###Overview
Transporting the radar equipment in good condition is essential to insure optimal operation in the field. Fixed wing aircraft, helicopter, truck, snow machine, people and even horses and yaks have all been used to transport the radar equipment. It is then important to protect some of the more fragile components such as described in the next sections.

###Transmitter antenna ports (Tx enclosure)
Ports are essential to lock-in the antenna protection while surveying. They are protected with the small green plug accessory that comes with the system (newer generation transmitting box only) on the outside, and a white screw cap on the inside. This also insures the transmit case remains waterproof when no antennas are connected.

###Antenna feed points
During transport the transmit antennas are disconnected from the transmitting case. As a result about 20cm of antenna wire comes out of the protection. Insure **no pulling or tugging** takes place on the antenna wires.

![Transport1](img/transport1.png)



* When putting away the antenna assembly, the **natural bending radius of the protective hose** should be followed while coiling it. It is important to not overbend the antenna wires especially where the resistor nodes are located.
* The RF connection of the antenna mid point is made out of a BNC connection. This connector should not be bend or crushed as it must conform to the mating BNC cable that links to the EPU input. A spare BNC-PP15 connector plug assembly is provided.
* All hose clamps, once untied for dismantling the system should be re-tied moderately for transport so that they do not slide off. This pertains to some of the protective hose ends, and ski/sled bindings.

![Transport2](img/transport2.png)


Also attachments for the battery box should be protected so that the locking wings are not damaged and locking straps are not lost during transport.

<img src="../img/batt_enclosure.jpg" width="400" height="300">	 <img src="../img/battbox Attach.png" width="200" height="300">