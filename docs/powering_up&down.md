#

###Powering Up
The power-up work flow is described below. The important thing to understand is that the EPU will start upon application of a 12-18V DC to its input terminals. Toggling the switch ON energizes the EPU's internal circuitry so it is good practice to insure the switch is in the OFF position when the battery is connected to the back of the EPU case.

As soon as a power source is connected the panel's analog voltage indicator will indicate the battery voltage and can be used as a proxy for the battery's available capacity. Because at this stage the switch should still be OFF, there is no current draw on the battery, and this can be considered an [Open Circuit Voltage](https://en.wikipedia.org/wiki/Open-circuit_voltage) battery reading. Once the switch is turned ON and current flows into the system, it is normal to observe a small voltage drop as shown by the indicator.

<img src="../img/poweringup_sheet.png" align="center">

###Powering Down

The power-down work flow is also important to insure a gracious OS shutdown. 

 <img src="../img/poweringdown_sheet.png" align="center">

!!! Warning
	Even though rare, there is always a chance the OS or data file become corrupted if a sudden loss of power occurs. To insure this is not the case, follow the "EPU Power OFF Procedure"