#

###Introduction

Batteries are a critical system component. Many factors make power management in the field more complex than in more controlled environments such as a lab or an office. They are:

   * remoteness of survey sites
   * battery weight
   * battery capacity
   * cold temperature with regards to discharging **and charging** the battery
    safe handling

###Battery chemistry

There are many types of chemistry available. The chemistry can either be [Gel](https://en.wikipedia.org/wiki/VRLA_battery#Gel_battery), or [LiFEPO4](http://en.wikipedia.org/wiki/Lithium_iron_phosphate_battery) or [Lithium-Ions](http://en.wikipedia.org/wiki/Lithium-ion_battery), each having different nominal operating voltages. LIFEP04 are better at addressing item 5 than Li-Ions, and their operating nominal voltage being 12.8V (resp. 14.8 V for Li-Ions) is closer to the 12V standard. Please read out the details about batteries on the links provided here. 

From our own field experience, we standardize on rechargeable lithium Ion. This chemistry addresses the first two items quite well. But in some cases reverting to Gel can be more practical.

!!!Warning
	Shipping Lithium batteries via air has many regulatory hurdles, mostly as the function of how many batteries are shipped at once. Please check regulatory procedures as they tend to change often.
	
 The type of Li-Ion batteries suggested in this page should meet directive PI966 - Section II of the IATA regulations, so they should be ok to ship by air **in non-passenger aircraft if shipped Cargo**. 


Lithium-based and SLA have been used for the EPU. 12-18VDC input is acceptable for nominal power. The EPU's battery should be no larger than the following SLA model: IT5-12, The physical size of the battery is important as it fits its carrying case. The IT5-12 should be the [sub-model F1](http://www.infinitybattery.com/files/I242-IT5-12_0901.pdf) (with connector type F1). The size is **3.54 x 2.76 x 4.02 inches (LxWxH)**. 

For other manufacturers, Panasonic for instance, L and W must be the same , while H can be a little longer by no more than 1" . It is recommended to have 2 such batteries for surveying, plus 1 spare for a total of 3. At 5A.h they would be expected to last up to 5 hours for 1A draw, but an estimate of ~ 3hrs is more accurate as it depends on outside air temperature, battery health, and how low the battery is depleted until it is swapped. For Lithium Ion, a 14.8V pack is best.

   - Tenergy Li-Ion 18650 14.8V 4400mAh PCB Protected Rechargeable Battery Pack
   - Tenergy Li-Ion 18650 14.8V 6600mAh Rechargeable Battery Pack w/ PCB Protection:
   - See these options [here](https://power.tenergy.com/battery-packs/li-ion-packs/14-8v/?_bc_fsnf=1&Capacity(mAh)[]=4400&Capacity(mAh)[]=6600&Capacity(mAh)[]=2200&Capacity(mAh)[]=2600).
   
LiFE-PO4 are now as common in smaller capacity depending on vendors. Some examples are given: 

   - Tenergy 12.8V 4.5Ah LiFePO4 Rechargeable Battery, PCB Protected
   - Tenergy 12.8V 3100mAh LiFePO4 Battery Pack, PCB Protected
   - See these options [here](https://power.tenergy.com/battery-packs/li-ion-packs/14-8v/?_bc_fsnf=1&Capacity(mAh)[]=4400&Capacity(mAh)[]=6600&Capacity(mAh)[]=2200&Capacity(mAh)[]=2600).

!!! Tip
	For all Lithium batteries it is critical to choose packs with less than 100Wh of energy if they have to be taken as carry-on. 	At 14.8V nominal, it corresponds to a battery capacity that is **no greater than 6600mAh** . See air transport regulation for changes.

###Chargers
Each battery chemistry has specific charging requirements and requires its own charger. At the very least, a charger must be checked for the chemistries it can handle.

[Charger examples](https://power.tenergy.com/chargers/for-battery-packs/3-7v-14-8v-li-ion-li-po/?_bc_fsnf=1)


###Batteries field operation
The external battery packs used in the system are most often Li-Ion or LiFePO4, the latter being most stable of all Lithium battery types to date. 

!!! Tip 
	All Li-Ion or LiFePo4 packs should come with an **internal protective circuit (IC)** whose role is to prevent runaway discharge in case of a short circuit. It also protects the battery from overcharging. 

Batteries must be handled with care to prevent damage of both the internal cells and of the protective IC that is mounted in the pack. Because of their greater energy density compared to SLA or NiCd batteries, the battery packs used with the radar system can be carried relatively easily and hence allow for longer surveys without the need of returning to base camp for recharging. Here is a list of important points with regards to Lithium batteries and their use in the field:

!!! Warning
	Battery may required a **certified packaging** before being shipped as Dangerous Goods.


!!! Tip
	Do not recharge batteries when their temperature < 0 oC. Best is to recharge > 5oC.

   - batteries should not be actively heated, but they can be rewarmed by placing them inside pockets, or at base camp in heated (hopefully) facilities.
   - Lithium batteries have a low-voltage cut-off (sudden drop to 0V). So the voltage level of the battery must be monitored accordingly to swap a new battery before the low-voltage cut-off takes effect.
   - if by mistake a lithium battery is shorted, its internal IC will cut-off the output voltage. If this happens, the only way to restore current flow is to put the battery on charge for a few minutes, so that the IC resets. If this was to happen during a survey, most likely the battery could not be reset unless a charger is at hand. Some charger may use a DC source to operate, some don't. So a AC source may be required.
   - Typically, and when provided, the radar system's external batteries are fitted with connectors minimizing the risk of short circuits and interconnection mistakes.


<img src="../img/Battery Charge and Temp.jpg" align="center">

###Additional information about discharge

It is recommended to become familiar with battery discharge curves which are chemistry-specific. Curves can be obtained from manufacturers. Discharge profiles are usually a function of current draw and temperature, as well as battery's history usage, and care of recharge procedure. Below are examples of such characteristics for an Infinity SLA Battery IT5-12 : 12V 5Ah.

<img src="../img/SLA_Discharge.jpg" align="center">


By contrast, the discharge curves of Lithium batteries (yellow ad red curves) are compared to SLA batteries (green and blue curves) on the figure below:

<img src="../img/ComputedDischarge.jpg" align="center">

 The x scale is time, and one can clearly see how the Li-ion batteries drop quickly and cut off at a certain time as indicated by the red arrows. A LiFePO4 chemistry would show a flatter characteristic, but would still switch-off due to the protective IC. Note that the operating time of each battery is not the point of the figure, both battery types having different overall capacities, hence the operating time difference.

###Typical current draw for the IceRadar equipment

Under 12V, current draws are typically as follows:

  - Transmitter: ~ 180mA while transmitting
  - EPU / Computer: 800mA (screen off) to 1A (screen on)


###Batteries Cost of ownership

   1. Even though lithium batteries cost more up-front, they do not require as much maintenance as the SLA/Gel types (maintenance may be in the form of ‘equalizing’ or ‘topping’ charge). If not done properly, an SLA battery may have to be replaced yearly.
   2. Lithium batteries can support more discharge cycles than SLAs.

(1) and (2) makes the overall cost of ownership more favorable for Li-Ions.


###Shipping Lithium Batteries

Shipping company, such as [DHL Express](https://www.dhl.com/en/express/shipping/shipping_advice/lithium_batteries.html#guides_materials) are a good source for Lithium battery shipping information as well as the [International Air Transport Association](https://www.iata.org/whatwedo/cargo/dgr/Pages/lithium-batteries.aspx)

Rules are different whether the batteries travel:

  - as cargo in cargo-only aircraft
  - as cargo in passenger aircraft
  - as carry-on : this option is often the simplest as long as each battery is < 100Wh, is wrapped properly, and connectors are protected.
    
CATSA guidelines for carry-on are [here](https://www.catsa-acsta.gc.ca/en/guidelines-batteries). Note the "Packing" section for spare batteries that emphasizes the following:

   - Don’t let a loose battery come into contact with metal objects (e.g. coins, keys, or jewelry).
   - Place each battery in a protective case, plastic bag, or leave it in its original packaging when possible. 
   - Add electrical tape on the battery's terminals to prevent short-circuits.
   - Prevent crushing, puncturing, or mechanical stress on the batteries.


!!! Note
	Latest 2019 IATA guidance document is [here](http://www.iata.org/whatwedo/cargo/dgr/Documents/lithium-battery-shipping-guidelines.pdf). 

