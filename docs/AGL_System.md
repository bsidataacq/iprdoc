#

### Introduction to AGL

"*In aviation, atmospheric sciences and broadcasting, a height above ground level (AGL or HAGL) is a height measured with respect to the underlying ground surface. This is as opposed to height above mean sea level (AMSL or HAMSL), height above ellipsoid (HAE, as reported by a GPS receiver), or height above average terrain (AAT or HAAT, in broadcast engineering). In other words, these expressions (AGL, AMSL, HAE, AAT) indicate where the "zero level" or "reference altitude" - the vertical datum - is located.* "

<img src="../img/agl_1.jpg" align="center">

In other words the AGL system measures the platform's clearance above the glacier surface.  

First, considering the variability of light conditions when flying above snow and ice-covered terrain the system can assist the pilot in the estimation of the platform position. Second, it provides a surface reference for the radar data as the latter tends to be obscured due to ringing within the few tens of meters below the aircraft. 

To achieve this the system is composed of: 

* a sensor module
* a link (display) module
* a data module  

### AGL Sensor Module

The sensor module must be installed with **no field of view obstruction** below the sensor (see specs below) looking down. The device includes a 24 GHz FMCW radar module for distance sensing as well as communication hardware for data transfer to the cockpit, and IPR unit.

 <img src="../img/agl_2.jpg" align="center">
 

|**Mechanical:** |**Electrical:**|
|--------|------------------------|
|size: 23x18x10cm|Vdc: 5 to 20V|
|weight: 750g , with battery|Current draw: ~ 165mA @ 14V|
|Waterproof, IP 65  (rain, dust) (*)    |Lithium rechargable battery 14.8 V Nominal, 4400mAh| 
|Mounting: tie-down and short sling |Battery duration: ~ 26 hr , de-rated to ~ 13 hr in cold conditions|

!!! Note
	(*): once lid is closed and latches are locked. 


|**Measurement Range** |**Resolution**| **Update Rate**|Req'd unobstruced fields of view|
|--------|------------------------|----|----|
| ~ 0.2-100m|~ 0.2m| 1 Hz| 28 Deg and 18 Deg |


<img src="../img/agl_5.jpg" align="center">

!!! Note
	Range is given for a good reflecting target, such as smooth ice, snow, water surfaces etc... Very uneven terrain would decrease the measurement range due to scattering. 

<img src="../img/agl_3.jpg" align="center">


 * the Power Port is connected to the onboard rechargeable battery
 * recharging the battery is done by disconnecting the leads from the power port and
   connecting the charger to the battery connectors.
 * The Battery life proxy indicator is given by the battery level:
 
 |**Full Battery** |** Half full**| ** Recharge Batt **|
|--------|------------------------|-----------|
|~ 16 V|~ 14.8 V| ~13.3 V|

Discharge curve for Li.Ions are typically showing a fast decline from Full state, to Nominal, then slow decline to around 13.3 V, then very fast again past this point when a cut-off point is reached.

Two Status LEDs are visible in the small window:

<img src="../img/agl_4.jpg" align="center">

|**Green LED** |** Orange LED Flashing**| 
|--------|------------------------|
|power on, system operating|a distance measurement is perfomed ($)|

!!! Note
	($): The distance sensor performs a measurement when a slight change in target's  position is detected. No measurement is taking place when the equipment is motionless.
	
### AGL Link Module

The Link Module is the display unit showing live read-outs from the data acquired by the Sensor Module. It includes a rechargeable lithium Ion battery.

<img src="../img/agl_link_1.jpg" align="center">


|**Mechanical:** |**Electrical:**|
|--------|------------------------|
|size: 19x15x5cm|Vdc: 5 to 20V|
|weight: 400g , with battery|Current draw: ~ 40mA @ 14V|
|Mounting: 1/4-20 thread (GoPro mount ok)  |Lithium rechargable battery 14.8 V Nominal, 2200mAh| 
|Mounting: velcro base available|Battery duration: ~56 hr, de-rated to ~28 hr (cold env.)|


<img src="../img/agl_link_2.jpg" align="center">

* Once the sensor module broadcasts a measurement, the link modules will display it. 
* If no data are received, **No Data** will show on the display
* The left side toggle switch selects the unit: feet or meters.
* The battery level indication is similar to the the sensor module'scattering
* The right side switch has **3 positions** : ON, OFF, and Batt.  
* The **Batt.** position should only be used for recharging the battery

<img src="../img/agl_link_3.jpg" align="center">

!!! Warning
	When the toggle switch is in the **Batt.** position, the battery is directly connected to the backside connector thatshould be protected by a small cap only to be removed when charging. Do not leave the switch on Batt. position when not connected to a charger. 
	

 |**Full Battery** |** Half full**| ** Recharge Batt **|
|--------|------------------------|-----------|
|~ 16 V|~ 14.8 V| ~13.3 V|

!!! Warning
	All battery recharging should ideally take place once the battery is at ambiant temperature (~ 20oC) and in any case must be above 5oC.

### AGL Data Module

The Data Module plugs directly into the USB front connector of the IceRadar Rx unit. The Air-IPR software interfaces with the module and merges the sensor's data into the hdf5 radar file.

<img src="../img/agl_data_1.jpg" align="center">