#

###Another HDF5 reader

  - Java-based HDF [file reader](http://www.hdfgroup.org/hdf-java-html/hdfview/) :  An utility to quickly look at the content of HDF5 fle data set and groups within a file.

###Basic Matlab examples to read IceRadar HDF5 data

  - [m-file](http://www.radar.bluesystem.ca/intro.m): A very basic set of examples to open and read IceRadar HDF5 data files.
  - [Matlab Library](http://www.radar.bluesystem.ca/hdf5tools.zip). Check the MathWorks website to look for update(s) on this library.

###Other 3rd party resources to work with IceRadar HDF5 files. These are as-is.

 - Ice Radar Library: [irlib](https://github.com/njwilson23/irlib): Ice Radar Library (irlib) is a set of software for handling [SFU Glaciology Group](http://www.sfu.ca/~gflowers/)'s radar data. Please acknowledge the author of the library if you use it.


###IceRadar Analyzer Updates

  - BSI's [download page](http://www.bluesystem.ca/downloads.html)