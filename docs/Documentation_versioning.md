
** Jan 2023**

- started AGL description 

** Nov 7.2021**

- added X-IceRadar dual channel acquisition 

** Nov 15.2019**

- added radargram examples for different ice regimes

**Oct 28.2019**

- off the ice testing

**Oct 25.2019**

- added snowsled section
- fixed link to LabVIEW-runtime engine

**Oct 15-23.2019**

- reorganize software section
- adding IceRadarAnalyzer content
- reorganize hardware section
- reorganize equipment section

**Aug 1-15.2019**

- complete clean up of all image files and text following data migration from previous repository
- migrate data from deprecated online repository