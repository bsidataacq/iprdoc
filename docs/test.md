# 

  <img src="../img/iceradar_3.png" align="center">

  
Understanding the response of glaciers and ice-sheets to climate, from thickness to changes to their flow regimes, is an essential step in evaluating climate change impacts on both the glacierized regions of the world and  beyond. In radio-glaciology, **ice-penetrating radar (IPR)** is one of scientists's most critical tools with applications ranging from ice-depth sounding to mapping the internal stratigraphy of ice sheets or inferring englacial and subglacial conditions.(Photo credit: Phil Hammer).   
 
Since 2008 BSI has been involved with academic partners in the cryospheric sciences to develop and produce a continuously evolving ice-penetrating radar (IPR) systems: the **IceRadar Platform**. Over the years, the system has been extensively used in its roving implementation which was developed for foot, skis, and land-based snow machines surveys in mountainous environment. 

<img src="../img/using_system1.png" align="center">

Most recently, the IceRadar was adapted for UAVs surveys (2018),  and several automated and stationary implementations (sIPR) were deployed in the St Elias Mountains and Canadian Arctic starting in 2014 (sIPR photo credit: Graham Clark).

  <img src="../img/uav_1.png" align="center">
  
  <img src="../img/sIPR_1.png" align="center">

And in 2021 a new helicopter-based airborne radar system was introduced:the **Air-IPR** :

<img src="../img/flying_1.jpg" align="center">

This documentation describes the different versions of the IceRadar system currently available as well as the **IceRadarAnalyzer software** for data analysis, filtering and bed picking. It is reviewed and updated to provide a dynamic source of information on the IPR systems. For more information, feel free to contact **info** at **bluesystem.ca** or via <a href="http://www.bluesystem.ca/contact.html" rel="nofollow" target="_blank">BSI's website</a>.

